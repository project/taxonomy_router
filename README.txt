
Taxonomy router
--------------
Page any branch of a taxonomy. 'Branch' means a term or a vocabulary. Then display either terms or nodes on the page.

NOTE: Only the direct (depth=1) children of a branch are displayed, not all descendants.

If the URL addresses a non-existent branch, even if URL form is valid, the pager returns a 'page not found' error.

Permissions
------------
Page viewing permitted by 'access content' (as Taxonomy).
Links in admin menus permitted by 'administer menu'.
Configuration permitted by 'administer site configuration'.

What is it good for?
--------------------
1) You can build a lot of interesting taxonomy code on top, and forget about the routing or theming. Just summon the right URL.

2) You can page terms, which you can't do in core. For example, if you just need a page that lists your forums, you can do it by loading this module then paging the base of your forums in the taxonomy.

How it works
------------
This diddy module makes a new router item of the form,

'?q=category/%'

The pager interprets the above, very general, router item as a taxonomy branch within your site taxonomy.

The URL uses somewhat the same address form as the taxonomy_menu module, except it's slacker... details follow.

The URL is of this form,
?q=category/[some digit representing a vocabulary, i.e. a vid]/[some digits with slash separators representing terms i.e. tids]

The way it works, the module looks for the last URL element. If it is the first element after 'category', it assumes the URL refers to a vocabulary. So this,

'category/3'

makes a page of node or term children from vocabulary (vid) 3.

If the element is not the first element, then it is assumed to be a term. So this,

'category/2/66/78967/2/1/3'

will make a page of nodes or terms which are children of term (tid) 3.

In the case of terms, the central sections are not regarded as significant. They must be valid vocabulary and term vids and tids, though. They are parsed to make sure they are digits and slashes, and are used to build the breadcrumb trail.

There are two more URL forms,

'category/nodes/[vid]/[tid]'

which pages nodes, as does the first URL form, and

'category/branches/[vid]/[tid]'

which pages links to terms.

Niether form will accept multiple tid lists, as will the 'category/...' form. But they will do this,

'category/nodes/3+566'
finds all the nodes in terms 3AND 566

'category/branches/5,75,233'
finds all the terms under terms 5 OR 75 OR 233


How it looks
------------
The lists of terms are designed to look very like lists of nodes, not an admin form. They even borrow some node CSS. However, they display the full titles of the vocabulary or term at the top.

The page titles are provided by page-branch-list.tpl.php.

Node lists are styled in tr-node-item.tpl.php. This is limited compared to a full node template, but should do. If anyone complains, I'll extend it. You can always access the $node variable for full node details, but don't forget to sanitise any output.

Term lists are styled by tr-term-item.tpl.php.



Some rationale
--------------
Always helps you make your mind up about something...

It differs from taxonomy_menu (the underlying code),
- the path root can't be renamed. It's always called 'category'. This could be changed, but that's not a priority.
- Recent versions of Taxonomy_menu create a huge listing of items in the router including, if a multiple hierarchy is enabled, multiple paths to the same item. This is very efficient codewise, and probably fast. I don't much like it because I thought the router was supposed to cut down on data bloat, by using generalised wildcarding - which is what Taxonomy Router does.
- It doesn't do anything else. It works as a sort of menu model, on which you can build for your own purposes.



Unpolished code
---------------
- I ripped out the functions handling the view module. They should probably be re-enstated at some stage.
- ...and it's got no rss feed?

