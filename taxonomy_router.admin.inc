<?php

/**
 * @file taxonomy_router.admin.inc
 * Configuration forms
 */

/**
 * Taxonomy router Settings page.
 *
 * @ingroup form
 */
 //doesn't default properly? Stings and integers form issues?
function taxonomy_router_settings() {
  $defaults = array('page_type'             => 'nodes',
                    'display_descendants'   => '0',
                    'descriptions_on_items' => TRUE,
                    );
  $settings = variable_get('taxonomy_router_settings', array());
  $settings = array_merge( $defaults, $settings );
  /*
  $form['taxonomy_router_menus'] = array(
    '#type'           => 'radios',
    '#title'          => t('Content type provided by urls'),
    '#options'        => array(
                           'terms_and_nodes' => t('Terms on branching urls, nodes on leaf urls.'), 
                           'nodes' => t('Nodes.'),
                           'terms' => t('Terms.'),
                         ),
    '#default_value'  => $settings['page_type'],
    '#description'    => 'Note that the "terms and nodes" option causes an extra database lookup, so is less efficient. The option is provided for users who wish to use taxonomy-router as a standalone module. It you are running taxonomy-router under the taxonomy-treemenu module, prefer that module\'s disable options.',
  );
  */
  
  $form['max_term_items_page'] = array(
    '#type' => 'select',
    '#title' => t('Number of terms on each page'),
    '#default_value'  =>  variable_get('default_terms_main', 10),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25, 30)),
  );
  

  $form['descriptions_on_items'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Show descriptions on branch item lists'),
    '#default_value'  => $settings['descriptions_on_items'],
    '#description'         => 'Switches off, in the default templates, term descriptions and node teasers. A fast method to change overall view behaviour, without messing with themes or templates.',
  );

  $form['#submit'][] = 'taxonomy_router_settings_submit';

  /*
  //could be useful if we provide a user definable url root
  $form['taxonomy_router_duplicated'] = array(
    '#type'           => 'textarea',
    '#title'          => t('Duplicated menu items'),
    '#description'    => t('You can specify a list of menu items that should be displayed twice: Once as a parent item that expands and collapses the sub-menu and again as its own child item, which points directly to the page (the parent items still links on double-click). Enter one internal path (like "admin") on each line. To specify a custom link text, put it after the path, separated by a space.'),
    '#rows'           => 4,
    '#default_value'  => variable_get('taxonomy_router_duplicated', taxonomy_router_DUPLICATE_DEFAULT),
  );
  */
  return system_settings_form($form);
}

function taxonomy_router_settings_submit($form, &$form_state) {
  //variable_del('taxonomy_router_menus');
//  $settings = variable_get('taxonomy_router_settings', array('page_type' => 'nodes', 'display_descendants' => '0',));
  $settings = array();
  
  $settings['page_type'] = $form_state['values']['taxonomy_router_menus'];
  $settings['descriptions_on_items'] = $form_state['values']['descriptions_on_items'];
  variable_set('default_terms_main', $form_state['values']['max_term_items_page']);
  
  variable_set('taxonomy_router_settings', $settings); 
  //drupal_set_message(t('Taxonomy treemenu settings changed.'));
}
